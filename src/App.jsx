import './App.css'
import { useTranslation } from 'react-i18next'

import { I18N } from './helpers'

function App() {
    const { t, i18n } = useTranslation('home')
    
    return (
        <>
            <select
                defaultValue={i18n.language}
                onChange={({target}) => 
                    I18N.changeLang(target.value, i18n)}>
                
                {I18N.availableLanguages.map(lng =>
                <option
                    key={lng.id}
                    id={lng.id} 
                    value={lng.id}>
                    {lng.description}
                </option>        
                )}

            </select>

            {t('welcome')}
        </>
    )
}

export default App
