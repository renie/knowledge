const I18N = {
    availableLanguages: [
        { id: 'en', description: 'English', dir: 'ltr' },
        { id: 'pt', description: 'Português', dir: 'ltr' },
        { id: 'uk', description: 'Українська', dir: 'ltr' },
    ],
    
    changeLang : (languageId, i18nLib) => {
        const lang = I18N.availableLanguages.find(lng => lng.id === languageId)
        if (!lang) return false

        document.documentElement.lang = lang.id
        document.documentElement.dir = lang.dir
        i18nLib.changeLanguage(lang.id)
    }
}

export default I18N

